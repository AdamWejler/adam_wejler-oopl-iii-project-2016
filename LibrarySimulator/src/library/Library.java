package library;

import java.util.Scanner;

import java.util.*;

public class Library {
	public LibraryMember member1 = new LibraryMember("Krzysztof Jackowski");
	public LibraryMember member2 = new LibraryMember("Anna Jaglak");
	public LibraryMember member3 = new LibraryMember("Adrianna Zak");
	public LibraryMember member4 = new LibraryMember("Zdzislaw Zalega");
	public LibraryMember member5 = new LibraryMember("Mariusz Krzeminski");
	public LibraryMember member6 = new LibraryMember("Sylwester Zagorski");
	
	public LibraryBook book1 = new LibraryBook("Zemsta", "A.Fredro", "comedy", 1);
	public LibraryBook book2 = new LibraryBook("Opowiesc Wigilijna", "K.Dickens", "tale", 2);
	public LibraryBook book3 = new LibraryBook("Krzyzacy", "H.Sienkiewicz", "historical novel", 3);
	public LibraryBook book4 = new LibraryBook("Odyseja", "Homer", "epos", 4);
	public LibraryBook book5 = new LibraryBook("Maly ksiaze", "A. de Saint � Exupery", "tale", 5);
	public LibraryBook book6 = new LibraryBook("Pan Tadeusz", "A.Mickiewicz", "epos", 6);
	public LibraryBook book7 = new LibraryBook("Romeo i Julia", "W. Szekspir", "tragedy", 7);
	public LibraryBook book8 = new LibraryBook("Folwark zwierzecy", "G. Orwell", "satire", 8);
	public LibraryBook book9 = new LibraryBook("Antygona", "Sofokles", "tragedy", 9);
	public LibraryBook book10 = new LibraryBook("Quo vadis", "H. Sienkiewicz", "historical novel", 10);
	
	public LinkedList<LibraryBook> listOfBooks = new LinkedList<LibraryBook>();
	
	public LinkedList<LibraryMember> listOfMembers = new LinkedList<LibraryMember>();
	
	public HashMap<LibraryMember, LibraryBook> booksReadByMembers = new HashMap<>();
	
	Library(){
		listOfBooks.add(book1);
		listOfBooks.add(book2);
		listOfBooks.add(book3);
		listOfBooks.add(book4);
		listOfBooks.add(book5);
		listOfBooks.add(book6);
		listOfBooks.add(book7);
		listOfBooks.add(book8);
		listOfBooks.add(book9);
		listOfBooks.add(book10);
		
		listOfMembers.add(member1);
		listOfMembers.add(member2);
		listOfMembers.add(member3);
		listOfMembers.add(member4);
		listOfMembers.add(member5);
		listOfMembers.add(member6);
		
		booksReadByMembers.put(member1, book1);
		booksReadByMembers.put(member2, book2);		
	}
	
	public void memberReadsBook(String memberName, String bookName){
		//Temporary variables
		LibraryBook b;
		LibraryMember m;
				
		for(LibraryMember member : listOfMembers){
			if(member.name.equals(memberName)){
				m = new LibraryMember(member.name);
				for(LibraryBook book : listOfBooks){
					if(book.title.equals(bookName)){
						b = new LibraryBook(book.title, book.author, book.cathegory, book.bookNumber);
						
						booksReadByMembers.put(m, b);
						break;
					}
				}
				break;
			}
		}
	}
	
	public void showBookReadByMember(String memberName){
		System.out.println(memberName + " read:");
				
		for(HashMap.Entry<LibraryMember, LibraryBook> entry : booksReadByMembers.entrySet()){
			if(entry.getKey().name.equals(memberName)){
		    	System.out.println((entry.getValue()).title);
		    }
		}
	}
	
	public void showAllBooks(){
		System.out.println("List of all books:");
		for(LibraryBook book : listOfBooks)
			System.out.println(book.title);
	}
	
	public void showAllMembers(){
		System.out.println("List of all members:");
		for(LibraryMember member : listOfMembers)
			System.out.println(member.name);
	}
	
	public void clear(){
		for(int i=0; i<20; i++)
			System.out.println();
	}
	
	public void showMenu1(){
		System.out.println("\t Operations to choose:");
		System.out.println("\t 1. Show all books.");
		System.out.println("\t 2. Show all members.");
		System.out.println("\t 3. Show books read by...");
		System.out.println("\t 4. Let the book be read by...");
	}
	
	public void showMenu2(){
		System.out.println("\t Books read by who? ...");
	}
	
	public void showMenu3(){
		System.out.println("\t Which book? ...");
	}
	
	public void showMenu4(){
		System.out.println("\t By who? ...");
	}
	
	public void runProgramMenu(){
		showMenu1();
		int input;
		while(true){
			try{
				Scanner scanner = new Scanner(System.in);
				input = scanner.nextInt();
				break;
			}catch(Exception e){
				System.out.println("\t  !!! Input proper data !!!");
			}
		}
			
		switch(input){
		case 1:
			clear();
			showAllBooks();
			System.out.println("\t Print Enter to back to menu.");
			Scanner scanner2 = new Scanner(System.in);
			String input2 = scanner2.nextLine();
			break;
		case 2:
			clear();
			showAllMembers();
			System.out.println("\t Print Enter to back to menu.");
			Scanner scanner3 = new Scanner(System.in);
			String input3 = scanner3.nextLine();
			break;
		case 3:
			showMenu2();
			String input4;
			while(true){
				try{
					Scanner scanner4 = new Scanner(System.in);
					input4 = scanner4.nextLine();
					break;
				}catch(Exception e){
					System.out.println("\t  !!! Input proper data !!!");
				}
			}
		
			showBookReadByMember(input4);
			System.out.println("\t Print Enter to back to menu.");
			
			String input7;
			while(true){
				try{
					Scanner scanner7 = new Scanner(System.in);
					input7 = scanner7.nextLine();
					break;
				}catch(Exception e){
					System.out.println("\t  !!! Input proper data !!!");
				}
			}
			
			break;
		case 4:
			showMenu3();
			String input5;
			while(true){
				try{
					Scanner scanner5 = new Scanner(System.in);
					input5 = scanner5.nextLine();
					break;
				}catch(Exception e){
					System.out.println("\t  !!! Input proper data !!!");
				}
			}
			showMenu4();
			String input6;
			while(true){
				try{
					Scanner scanner6 = new Scanner(System.in);
					input6 = scanner6.nextLine();
					break;
				}catch(Exception e){
					System.out.println("\t  !!! Input proper data !!!");
				}
			}
			memberReadsBook(input6, input5);
			break;	
		}
		clear();
		runProgramMenu();
	}
}

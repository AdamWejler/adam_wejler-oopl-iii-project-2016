package library;

public class LibraryBook extends Book implements PagesSpecyfication, ChangeableCathegory {
	int bookNumber;

	@Override
	public void changeBookCathegory(String newName) {
		this.cathegory = newName;
	}

	LibraryBook(String title, String author, String cathegory, int bookNumber){
		this.resourceCategory = "Book";
		this.title = title;
		this.author = author;
		this.cathegory = cathegory;
		this.bookNumber = bookNumber;
	}
}
